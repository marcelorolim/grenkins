def colors (string color) {
    if ('SUCCESS' == color) {
        return 'good'
    }
    return 'danger'
}

def user() {
    return currentBuild.rawBuild.getCause(Cause.UserIdCause).getUserId()
}

def branch() {
    regurn scm.branches[0].name
}

def commit() {
    return sh (script: "git log -n 1 --pretty=format:'%H'", returnStdout: true)
}

def call(String canal, String status, String colo_status) {
    slackSend channel: canal,
    color: colors(color_status),
    message: "*${status}* pipeline ${env.JOB_NAME} \n
    branch: ${env.BUILD_NUMBER} by *${user()}*
    \n build ${env.BUILD_NUMBER} by *${user()}*
    \n More info at: ${env.BUILD_URL}"
}